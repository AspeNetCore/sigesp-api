# .NET Core SDK
FROM mcr.microsoft.com/dotnet/core/sdk:5.0-alpine AS build

# Sets the working directory
WORKDIR /app

# Copy Projects
#COPY *.sln .
COPY Src/Sigesp.Application/Sigesp.Application.csproj ./Sigesp.Application/
COPY Src/Sigesp.Domain/Sigesp.Domain.csproj ./Sigesp.Domain/
COPY Src/Sigesp.Domain.Core/Sigesp.Domain.Core.csproj ./Sigesp.Domain.Core/
COPY Src/Sigesp.Infra.CrossCutting.Bus/Sigesp.Infra.CrossCutting.Bus.csproj ./Sigesp.Infra.CrossCutting.Bus/
COPY Src/Sigesp.Infra.CrossCutting.Identity/Sigesp.Infra.CrossCutting.Identity.csproj ./Sigesp.Infra.CrossCutting.Identity/
COPY Src/Sigesp.Infra.CrossCutting.IoC/Sigesp.Infra.CrossCutting.IoC.csproj ./Sigesp.Infra.CrossCutting.IoC/
COPY Src/Sigesp.Infra.Data/Sigesp.Infra.Data.csproj ./Sigesp.Infra.Data/
COPY Src/Sigesp.Services.Api/Sigesp.Services.Api.csproj ./Sigesp.Services.Api/

# .NET Core Restore
RUN dotnet restore ./Sigesp.Services.Api/Sigesp.Services.Api.csproj

# Copy All Files
COPY Src ./

# .NET Core Build and Publish
RUN dotnet publish ./Sigesp.Services.Api/Sigesp.Services.Api.csproj -c Release -o /publish

# ASP.NET Core Runtime
FROM mcr.microsoft.com/dotnet/core/aspnet:5.0-alpine AS runtime
WORKDIR /app
COPY --from=build /publish ./
#EXPOSE 80
#EXPOSE 443
ENTRYPOINT ["dotnet", "Sigesp.Services.Api.dll"]
