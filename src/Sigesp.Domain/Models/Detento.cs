using System;
using Sigesp.Domain.Core.Models;

namespace Sigesp.Domain.Models
{
    public class Detento : EntityAudit
    {
        public Detento(int detentoId, int detentoIpen, string detentoNome, int detentoDataCriacao)
        {
            DetentoId  = detentoId;
            DetentoIpen = detentoIpen;
            DetentoNome = detentoNome;
            DetentoDataCriacao = detentoDataCriacao;
        }

        // Empty constructor for EF
        protected Detento() { }

        public int DetentoId { get; private set; }

        public int DetentoIpen { get; private set; }
        
        public string DetentoNome { get; private set; }

        public int DetentoDataCriacao { get; private set; }

        // public string DetentoGaleria { get; set; }
        // public int DetentoCela { get; set; }        
        // public string DetentoCpf { get; set; }        
        // public string DetentoRg { get; set; }
        // public int DetentoDataNascimento { get; set; }
        // public string DetentoPec { get; set; }
        // public bool DetentoIsProvisorio { get; set; }
        // public bool DetentoIsAtivo { get; set; }
        // public int DetentoDataUltimaAtualizacao { get; set; }
    }
}