using Sigesp.Domain.Services.Mail;

namespace Sigesp.Domain.Services
{
    public interface IMailService
    {
        void SendMail(MailMessage mailMessage);
    }
}
