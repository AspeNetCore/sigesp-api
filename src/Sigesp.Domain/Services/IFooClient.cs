using System.Threading.Tasks;
using Refit;

namespace Sigesp.Domain.Services
{
    public interface IFooClient
    {
        [Get("/")]
        Task<object> GetAll();
    }
}
