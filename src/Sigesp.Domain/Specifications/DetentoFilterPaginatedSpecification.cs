using Sigesp.Domain.Models;

namespace Sigesp.Domain.Specifications
{
    public class DetentoFilterPaginatedSpecification : BaseSpecification<Detento>
    {
        public DetentoFilterPaginatedSpecification(int skip, int take)
            : base(i => true)
        {
            ApplyPaging(skip, take);
        }
    }
}