using System.Collections.Generic;
using System.Threading.Tasks;
using Sigesp.Domain.Models;
 
 namespace Sigesp.Domain.Interfaces
 {
    public interface IDetentoRepository : IRepository<Detento>
     {
        
     }
}