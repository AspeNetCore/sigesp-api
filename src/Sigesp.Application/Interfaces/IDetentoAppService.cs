using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sigesp.Domain.Models;
using Sigesp.Application.ViewModels;
using FluentValidation.Results;

namespace Sigesp.Application.Interfaces
{
    public interface IDetentoAppService : IDisposable
    {
        IEnumerable<DetentoViewModel> GetAll();
        void Register(DetentoViewModel detentoViewModel);
    }
}