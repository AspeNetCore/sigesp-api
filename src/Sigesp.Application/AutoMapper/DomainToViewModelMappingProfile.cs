using AutoMapper;
using Sigesp.Application.ViewModels;
using Sigesp.Domain.Models;

namespace Sigesp.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Customer, CustomerViewModel>();
        }
    }
}
