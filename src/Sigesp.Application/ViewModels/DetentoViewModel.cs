namespace Sigesp.Application.ViewModels
{
    public class DetentoViewModel
    {
        public int DetentoId { get; set; }
        
        public int DetentoIpen { get; set; }
        
        public string DetentoNome { get; set; }
        
        public int DetentoDataCriacao { get; set; }
    }
}