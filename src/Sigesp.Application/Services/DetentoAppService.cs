using System;
using System.Collections.Generic;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Sigesp.Application.EventSourcedNormalizers;
using Sigesp.Application.Interfaces;
using Sigesp.Application.ViewModels;
using Sigesp.Domain.Commands;
using Sigesp.Domain.Core.Bus;
using Sigesp.Domain.Interfaces;
using Sigesp.Domain.Specifications;
using Sigesp.Infra.Data.Repository.EventSourcing;

namespace Sigesp.Application.Services
{
    public class DetentoAppService : IDetentoAppService
    {
        private readonly IMapper _mapper;
        private readonly IDetentoRepository _detentoRepository;
        private readonly IEventStoreRepository _eventStoreRepository;
        private readonly IMediatorHandler Bus;

        public DetentoAppService(IMapper mapper,
                                  IDetentoRepository detentoRepository,
                                  IMediatorHandler bus,
                                  IEventStoreRepository eventStoreRepository)
        {
            _mapper = mapper;
            _detentoRepository = detentoRepository;
            Bus = bus;
            _eventStoreRepository = eventStoreRepository;
        }

        public IEnumerable<DetentoViewModel> GetAll()
        {
            return _detentoRepository.GetAll().ProjectTo<DetentoViewModel>(_mapper.ConfigurationProvider);
        }

        public IEnumerable<DetentoViewModel> GetAll(int skip, int take)
        {
            return _detentoRepository.GetAll(new DetentoFilterPaginatedSpecification(skip, take))
                .ProjectTo<DetentoViewModel>(_mapper.ConfigurationProvider);
        }

        public DetentoViewModel GetById(Guid id)
        {
            return _mapper.Map<DetentoViewModel>(_detentoRepository.GetById(id));
        }

        public void Register(DetentoViewModel detentoViewModel)
        {
            var registerCommand = _mapper.Map<RegisterNewCustomerCommand>(detentoViewModel);
            Bus.SendCommand(registerCommand);
        }

        public void Update(DetentoViewModel detentoViewModel)
        {
            var updateCommand = _mapper.Map<UpdateCustomerCommand>(detentoViewModel);
            Bus.SendCommand(updateCommand);
        }

        public void Remove(Guid id)
        {
            var removeCommand = new RemoveCustomerCommand(id);
            Bus.SendCommand(removeCommand);
        }

        public IList<DetentoHistoryData> GetAllHistory(Guid id)
        {
            return DetentoHistory.ToJavaScriptCustomerHistory(_eventStoreRepository.All(id));
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}