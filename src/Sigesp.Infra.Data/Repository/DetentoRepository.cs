using System.Linq;
using Sigesp.Domain.Interfaces;
using Sigesp.Domain.Models;
using Sigesp.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace Sigesp.Infra.Data.Repository
{
    public class DetentoRepository : Repository<Detento>, IDetentoRepository
    {
        public DetentoRepository(SigespDbContext context)
            : base(context)
        {

        }
    }
}
