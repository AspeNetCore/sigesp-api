using System;
using System.Collections.Generic;
using Sigesp.Domain.Core.Events;

namespace Sigesp.Infra.Data.Repository.EventSourcing
{
    public interface IEventStoreRepository : IDisposable
    {
        void Store(StoredEvent theEvent);
        IList<StoredEvent> All(Guid aggregateId);
    }
}
