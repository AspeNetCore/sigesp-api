using Sigesp.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sigesp.Infra.Data.Mappings
{
    public class DetentoMap : IEntityTypeConfiguration<Detento>
    {
        public void Configure(EntityTypeBuilder<Detento> builder)
        {
            builder.ToTable("Detentos");
            
            builder.HasKey(c => c.DetentoId);               

            builder.Property(c => c.DetentoId)
                .HasColumnName("DetentoId")
                .ValueGeneratedOnAdd();

            builder.Property(c => c.DetentoIpen)
                .HasColumnName("DetentoIpen");

            builder.Property(c => c.DetentoNome)
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(c => c.DetentoDataCriacao)   
                .HasColumnName("DetentoDataCriacao")
                .IsRequired();
        }
    }
}