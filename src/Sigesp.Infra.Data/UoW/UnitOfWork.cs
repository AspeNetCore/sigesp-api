using Sigesp.Domain.Interfaces;
using Sigesp.Infra.Data.Context;

namespace Sigesp.Infra.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SigespDbContext _context;

        public UnitOfWork(SigespDbContext context)
        {
            _context = context;
        }

        public bool Commit()
        {
            return _context.SaveChanges() > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
