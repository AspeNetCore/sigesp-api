using System;
using Sigesp.Application.Interfaces;
using Sigesp.Application.ViewModels;
using Sigesp.Domain.Core.Bus;
using Sigesp.Domain.Core.Notifications;
using Sigesp.Infra.CrossCutting.Identity.Authorization;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Sigesp.Services.Api.Controllers
{
    [Authorize]
    public class DetentoController : ApiController
    {
        private readonly IDetentoAppService _detentoAppService;

        public DetentoController(IDetentoAppService detentoAppService,
                                INotificationHandler<DomainNotification> notifications,
                                IMediatorHandler mediator) : base(notifications, mediator) 
        {
            _detentoAppService = detentoAppService;
        } 

        [HttpGet]
        [AllowAnonymous]
        [Route("detento-management")]
        public IActionResult Get()
        {
            return Response(_detentoAppService.GetAll());
        }

        [HttpPost]
        [Authorize(Policy = "CanWriteDetentoData", Roles = Roles.Admin)]
        [Route("detento-management")]
        public IActionResult Post([FromBody]DetentoViewModel detentoViewModel)
        {
            if (!ModelState.IsValid)
            {
                NotifyModelStateErrors();
                return Response(detentoViewModel);
            }

            _detentoAppService.Register(detentoViewModel);

            return Response(detentoViewModel);
        }
    }
}