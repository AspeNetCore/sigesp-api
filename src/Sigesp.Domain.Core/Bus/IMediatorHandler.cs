using System.Threading.Tasks;
using Sigesp.Domain.Core.Commands;
using Sigesp.Domain.Core.Events;

namespace Sigesp.Domain.Core.Bus
{
    public interface IMediatorHandler
    {
        Task SendCommand<T>(T command) where T : Command;
        Task RaiseEvent<T>(T @event) where T : Event;
    }
}
